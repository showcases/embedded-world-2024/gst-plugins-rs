// Copyright (C) 2024 Benjamin Gaignard <benjamin.gaignard@collabora.com>
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.
//
// SPDX-License-Identifier: MIT OR Apache-2.0

use gst::glib;
use gst::prelude::*;
use gst::subclass::prelude::*;
use gst_analytics::*;
use once_cell::sync::Lazy;
use std::collections::HashSet;

pub struct OnvifMeta2RelationMeta {
    // Input media stream with ONVIF metadata
    sinkpad: gst::Pad,
    // Output media stream with relation metadata
    srcpad: gst::Pad,
}

static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "onvifmeta2relationmeta",
        gst::DebugColorFlags::empty(),
        Some("ONVIF metadata to Relation meta"),
    )
});

#[glib::object_subclass]
impl ObjectSubclass for OnvifMeta2RelationMeta {
    const NAME: &'static str = "GstOnvifMeta2RelationMeta";
    type Type = super::OnvifMeta2RelationMeta;
    type ParentType = gst::Element;

    fn with_class(klass: &Self::Class) -> Self {
        let templ = klass.pad_template("sink").unwrap();
        let sinkpad = gst::Pad::builder_from_template(&templ)
            .chain_function(|pad, parent, buffer| {
                OnvifMeta2RelationMeta::catch_panic_pad_function(
                    parent,
                    || Err(gst::FlowError::Error),
                    |convert| convert.sink_chain(pad, buffer),
                )
            })
            .flags(gst::PadFlags::PROXY_CAPS)
            .build();
        let templ = klass.pad_template("src").unwrap();
        let srcpad = gst::Pad::builder_from_template(&templ)
            .flags(gst::PadFlags::PROXY_CAPS)
            .build();

        Self { srcpad, sinkpad }
    }
}

impl ObjectImpl for OnvifMeta2RelationMeta {
    fn constructed(&self) {
        self.parent_constructed();

        let obj = self.obj();
        obj.add_pad(&self.sinkpad).unwrap();
        obj.add_pad(&self.srcpad).unwrap();
    }
}

impl GstObjectImpl for OnvifMeta2RelationMeta {}

impl ElementImpl for OnvifMeta2RelationMeta {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "ONVIF metadata to relation metadata",
                "Metadata",
                "Convert ONVIF metadata to relation metadata",
                "Benjamin Gaignard <benjamin.gaignard@collabora.com",
            )
        });

        Some(&*ELEMENT_METADATA)
    }

    fn pad_templates() -> &'static [gst::PadTemplate] {
        static PAD_TEMPLATES: Lazy<Vec<gst::PadTemplate>> = Lazy::new(|| {
            let sink_caps = gst::Caps::new_any();
            let sink_pad_template = gst::PadTemplate::new(
                "sink",
                gst::PadDirection::Sink,
                gst::PadPresence::Always,
                &sink_caps,
            )
            .unwrap();

            let src_caps = gst::Caps::new_any();
            let src_pad_template = gst::PadTemplate::new(
                "src",
                gst::PadDirection::Src,
                gst::PadPresence::Always,
                &src_caps,
            )
            .unwrap();

            vec![src_pad_template, sink_pad_template]
        });

        PAD_TEMPLATES.as_ref()
    }
}

impl OnvifMeta2RelationMeta {
    fn sink_chain(
        &self,
        pad: &gst::Pad,
        mut input_buffer: gst::Buffer,
    ) -> Result<gst::FlowSuccess, gst::FlowError> {
        if let Ok(metas) =
            gst::meta::CustomMeta::from_buffer(input_buffer.get_mut().unwrap(), "OnvifXMLFrameMeta")
        {
            let s = metas.structure();

            if let Ok(frames) = s.get::<gst::BufferList>("frames") {
                let mut object_ids = HashSet::new();

                for buffer in frames.iter().rev() {
                    let buffer = buffer.map_readable().map_err(|_| {
                        gst::element_imp_error!(
                            self,
                            gst::ResourceError::Read,
                            ["Failed to map buffer readable"]
                        );

                        gst::FlowError::Error
                    })?;

                    let utf8 = std::str::from_utf8(buffer.as_ref()).map_err(|err| {
                        gst::element_imp_error!(
                            self,
                            gst::StreamError::Format,
                            ["Failed to decode buffer as UTF-8: {}", err]
                        );

                        gst::FlowError::Error
                    })?;

                    let root =
                        xmltree::Element::parse(std::io::Cursor::new(utf8)).map_err(|err| {
                            gst::element_imp_error!(
                                self,
                                gst::StreamError::Decode,
                                ["Failed to parse buffer as XML: {}", err]
                            );

                            gst::FlowError::Error
                        })?;

                    for object in root
                        .get_child(("VideoAnalytics", crate::ONVIF_METADATA_SCHEMA))
                        .map(|e| e.children.iter().filter_map(|n| n.as_element()))
                        .into_iter()
                        .flatten()
                    {
                        if object.name == "Frame"
                            && object.namespace.as_deref() == Some(crate::ONVIF_METADATA_SCHEMA)
                        {
                            for object in object
                                .children
                                .iter()
                                .filter_map(|n| n.as_element())
                                .filter(|e| {
                                    e.name == "Object"
                                        && e.namespace.as_deref()
                                            == Some(crate::ONVIF_METADATA_SCHEMA)
                                })
                            {
                                gst::trace!(CAT, obj: pad, "Handling object {:?}", object);

                                let object_id = match object.attributes.get("ObjectId") {
                                    Some(id) => id.to_string(),
                                    None => {
                                        gst::warning!(
                                            CAT,
                                            imp: self,
                                            "XML Object with no ObjectId"
                                        );
                                        continue;
                                    }
                                };

                                if !object_ids.insert(object_id.clone()) {
                                    gst::debug!(
                                        CAT,
                                        "Skipping older version of object {}",
                                        object_id
                                    );
                                    continue;
                                }

                                let appearance = match object
                                    .get_child(("Appearance", crate::ONVIF_METADATA_SCHEMA))
                                {
                                    Some(appearance) => appearance,
                                    None => {
                                        gst::warning!(
                                            CAT,
                                            imp: self,
                                            "XML Object with no Appearance"
                                        );
                                        continue;
                                    }
                                };

                                let shape = match appearance
                                    .get_child(("Shape", crate::ONVIF_METADATA_SCHEMA))
                                {
                                    Some(shape) => shape,
                                    None => {
                                        gst::warning!(
                                            CAT,
                                            imp: self,
                                            "XML Object with no Shape"
                                        );
                                        continue;
                                    }
                                };

                                let class = match appearance
                                    .get_child(("Class", crate::ONVIF_METADATA_SCHEMA))
                                {
                                    Some(class) => class,
                                    None => {
                                        gst::warning!(
                                            CAT,
                                            imp: self,
                                            "XML Object with no Class"
                                        );
                                        continue;
                                    }
                                };

                                let t =
                                    match class.get_child(("Type", crate::ONVIF_METADATA_SCHEMA)) {
                                        Some(t) => t,
                                        None => {
                                            gst::warning!(
                                                CAT,
                                                imp: self,
                                                "XML Class with no Type"
                                            );
                                            continue;
                                        }
                                    };

                                let likelihood: f64 = match t
                                    .attributes
                                    .get("Likelihood")
                                    .and_then(|val| val.parse().ok())
                                {
                                    Some(val) => val,
                                    None => {
                                        gst::warning!(
                                            CAT,
                                            imp: self,
                                            "Type with no Likelihood attribute"
                                        );
                                        continue;
                                    }
                                };

                                let tag: String = match t.get_text() {
                                    Some(tag) => tag.to_string(),
                                    None => {
                                        gst::warning!(
                                            CAT,
                                            imp: self,
                                            "XML Type with no text"
                                        );
                                        continue;
                                    }
                                };

                                let bbox = match shape
                                    .get_child(("BoundingBox", crate::ONVIF_METADATA_SCHEMA))
                                {
                                    Some(bbox) => bbox,
                                    None => {
                                        gst::warning!(
                                            CAT,
                                            imp: self,
                                            "XML Shape with no BoundingBox"
                                        );
                                        continue;
                                    }
                                };

                                let left: f64 = match bbox
                                    .attributes
                                    .get("left")
                                    .and_then(|val| val.parse().ok())
                                {
                                    Some(val) => val,
                                    None => {
                                        gst::warning!(
                                            CAT,
                                            imp: self,
                                            "BoundingBox with no left attribute"
                                        );
                                        continue;
                                    }
                                };

                                let right: f64 = match bbox
                                    .attributes
                                    .get("right")
                                    .and_then(|val| val.parse().ok())
                                {
                                    Some(val) => val,
                                    None => {
                                        gst::warning!(
                                            CAT,
                                            imp: self,
                                            "BoundingBox with no right attribute"
                                        );
                                        continue;
                                    }
                                };

                                let top: f64 = match bbox
                                    .attributes
                                    .get("top")
                                    .and_then(|val| val.parse().ok())
                                {
                                    Some(val) => val,
                                    None => {
                                        gst::warning!(
                                            CAT,
                                            imp: self,
                                            "BoundingBox with no top attribute"
                                        );
                                        continue;
                                    }
                                };

                                let bottom: f64 = match bbox
                                    .attributes
                                    .get("bottom")
                                    .and_then(|val| val.parse().ok())
                                {
                                    Some(val) => val,
                                    None => {
                                        gst::warning!(
                                            CAT,
                                            imp: self,
                                            "BoundingBox with no bottom attribute"
                                        );
                                        continue;
                                    }
                                };

                                gst::log!(
				    CAT,
				    imp: self,
				    "Object detected with label : {}, likelihood: {}, bounding box: {}x{} at ({},{})",
				    tag, likelihood, (right - left), (bottom - top), left, top);

                                let mut arm =
                                    AnalyticsRelationMeta::add(input_buffer.get_mut().unwrap());
                                let _ = arm.add_od_mtd(
                                    glib::Quark::from_str(tag.clone()),
                                    left as i32,
                                    top as i32,
                                    (bottom - top) as i32,
                                    (right - left) as i32,
                                    likelihood as f32,
                                );
                                let _ = arm
                                    .add_one_cls_mtd(likelihood as f32, glib::Quark::from_str(tag));
                            }
                        }
                    }
                }
            }
        }

        if let Ok(metas) = gst::meta::CustomMeta::from_mut_buffer(
            input_buffer.get_mut().unwrap(),
            "OnvifXMLFrameMeta",
        ) {
            metas.remove().unwrap();
        }

        self.srcpad.push(input_buffer)
    }
}
