// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0

use gst::glib;
use gst::prelude::*;
use gst::subclass::prelude::*;
use once_cell::sync::Lazy;
use std::ops::ControlFlow;

static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "onvifmetadataseparator",
        gst::DebugColorFlags::empty(),
        Some("ONVIF Metadata Separator Element"),
    )
});

pub struct OnvifMetadataSeparator {
    // Input media stream
    sink_pad: gst::Pad,
    // Output media stream
    media_src_pad: gst::Pad,
    // Output metadata stream, complete VideoAnalytics XML documents
    meta_src_pad: gst::Pad,
}

#[glib::object_subclass]
impl ObjectSubclass for OnvifMetadataSeparator {
    const NAME: &'static str = "GstOnvifMetadataSeparator";
    type Type = super::OnvifMetadataSeparator;
    type ParentType = gst::Element;

    fn with_class(klass: &Self::Class) -> Self {
        let templ = klass.pad_template("sink").unwrap();
        let sink_pad = gst::Pad::builder_from_template(&templ)
            .chain_function(|pad, parent, buffer| {
                OnvifMetadataSeparator::catch_panic_pad_function(
                    parent,
                    || Err(gst::FlowError::Error),
                    |separator| separator.sink_chain(pad, buffer),
                )
            })
            .event_function(|pad, parent, event| {
                OnvifMetadataSeparator::catch_panic_pad_function(
                    parent,
                    || false,
                    |separator| separator.sink_event(pad, event),
                )
            })
            .build();
        let templ = klass.pad_template("media_src").unwrap();
        let media_src_pad = gst::Pad::builder_from_template(&templ).build();
        let templ = klass.pad_template("meta_src").unwrap();
        let meta_src_pad = gst::Pad::builder_from_template(&templ).build();

        Self {
            sink_pad,
            media_src_pad,
            meta_src_pad,
        }
    }
}

impl ObjectImpl for OnvifMetadataSeparator {
    fn constructed(&self) {
        self.parent_constructed();

        let obj = self.obj();
        obj.add_pad(&self.sink_pad).unwrap();
        obj.add_pad(&self.media_src_pad).unwrap();
        obj.add_pad(&self.meta_src_pad).unwrap();
    }
}

impl GstObjectImpl for OnvifMetadataSeparator {}

impl ElementImpl for OnvifMetadataSeparator {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "ONVIF metadata separator",
                "Video/Metadata",
                "Separates the ONVIF GstMeta into a separate stream",
                "Benjamin Gaignard <benjamin.gaignard@collabora.com",
            )
        });

        Some(&*ELEMENT_METADATA)
    }

    fn pad_templates() -> &'static [gst::PadTemplate] {
        static PAD_TEMPLATES: Lazy<Vec<gst::PadTemplate>> = Lazy::new(|| {
            let sink_caps = gst::Caps::new_any();
            let sink_pad_template = gst::PadTemplate::new(
                "sink",
                gst::PadDirection::Sink,
                gst::PadPresence::Always,
                &sink_caps,
            )
            .unwrap();

            let media_src_pad_template = gst::PadTemplate::new(
                "media_src",
                gst::PadDirection::Src,
                gst::PadPresence::Always,
                &sink_caps,
            )
            .unwrap();

            let meta_caps = gst::Caps::builder("application/x-onvif-metadata").build();
            let meta_src_pad_template = gst::PadTemplate::new(
                "meta_src",
                gst::PadDirection::Src,
                gst::PadPresence::Always,
                &meta_caps,
            )
            .unwrap();

            vec![
                sink_pad_template,
                media_src_pad_template,
                meta_src_pad_template,
            ]
        });

        PAD_TEMPLATES.as_ref()
    }
}

impl OnvifMetadataSeparator {
    fn sink_event(&self, pad: &gst::Pad, event: gst::Event) -> bool {
        use gst::EventView;

        gst::log!(CAT, obj: pad, "Handling event {:?}", event);

        match event.view() {
            EventView::Caps(..) => {
                self.media_src_pad.push_event(event);
                let caps = &self.meta_src_pad.pad_template_caps();
                self.meta_src_pad.push_event(gst::event::Caps::new(&caps))
            }
            _ => gst::Pad::event_default(pad, Some(&*self.obj()), event),
        }
    }

    fn sink_chain(
        &self,
        pad: &gst::Pad,
        buffer: gst::Buffer,
    ) -> Result<gst::FlowSuccess, gst::FlowError> {
        gst::log!(CAT, obj: pad, "Handling buffer {:?}", buffer);

        if let Ok(metas) =
            gst::meta::CustomMeta::from_buffer(&buffer, "OnvifXMLFrameMeta")
        {
            let s = metas.structure();

            if let Ok(frames) = s.get::<gst::BufferList>("frames") {
                frames.foreach(|meta_buf, _idx| {
                    if let Ok(_) = self.meta_src_pad.push(meta_buf.clone()) {
                        ControlFlow::Continue(())
                    } else {
                        ControlFlow::Break(())
                    }
                });
            }
        }

        self.media_src_pad.push(buffer)
    }
}
