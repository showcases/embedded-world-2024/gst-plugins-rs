// Copyright (C) 2024 Collabora Ltd
//   @author: Olivier Crête <olivier.crete@collabora.com>
//
// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0

use gst::glib;
use gst::prelude::*;
use gst::subclass::prelude::*;

use crate::originalbuffermeta::OriginalBufferMeta;

#[derive(Default)]
struct State {
    interval: gst::ClockTime,
    last_running_time: Option<gst::ClockTime>,
}

pub struct OriginalBufferSave {
    src_pad: gst::Pad,
    sink_pad: gst::Pad,
    state: std::sync::Mutex<State>,
}

use once_cell::sync::Lazy;
static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "originalbuffersave",
        gst::DebugColorFlags::empty(),
        Some("Save Original buffer as meta"),
    )
});

#[glib::object_subclass]
impl ObjectSubclass for OriginalBufferSave {
    const NAME: &'static str = "GstOriginalBufferSave";
    type Type = super::OriginalBufferSave;
    type ParentType = gst::Element;

    fn with_class(klass: &Self::Class) -> Self {
        let sink_templ = klass.pad_template("sink").unwrap();
        let src_templ = klass.pad_template("src").unwrap();

        let sink_pad = gst::Pad::builder_from_template(&sink_templ)
            .chain_function(|pad, parent, buffer| {
                OriginalBufferSave::catch_panic_pad_function(
                    parent,
                    || Err(gst::FlowError::Error),
                    |obj| obj.sink_chain(pad, buffer),
                )
            })
            .event_function(|pad, parent, event| {
                OriginalBufferSave::catch_panic_pad_function(
                    parent,
                    || false,
                    |obj| obj.sink_event(pad, parent, event),
                )
            })
            .query_function(|pad, parent, query| {
                OriginalBufferSave::catch_panic_pad_function(
                    parent,
                    || false,
                    |obj| obj.sink_query(pad, parent, query),
                )
            })
            .flags(gst::PadFlags::PROXY_CAPS | gst::PadFlags::PROXY_ALLOCATION)
            .build();

        let src_pad = gst::Pad::builder_from_template(&src_templ)
            .event_function(|pad, parent, event| {
                OriginalBufferSave::catch_panic_pad_function(
                    parent,
                    || false,
                    |obj| obj.src_event(pad, parent, event),
                )
            })
            .build();

        Self {
            src_pad,
            sink_pad,
            state: Default::default(),
        }
    }
}

impl ObjectImpl for OriginalBufferSave {
    fn constructed(&self) {
        self.parent_constructed();

        let obj = self.obj();
        obj.add_pad(&self.sink_pad).unwrap();
        obj.add_pad(&self.src_pad).unwrap();
    }

    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
            vec![
                glib::ParamSpecUInt64::builder("interval")
                    .nick("Interval")
                    .blurb("Time interval between two forwarded buffers, replaces buffers with GAP events under this minimum, acculates slack")
		    .minimum(0)
		    .maximum(std::u64::MAX)
                    .default_value(0)
		    .mutable_playing()
                    .build()
            ]
        });

        PROPERTIES.as_ref()
    }

    fn set_property(&self, _id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
        match pspec.name() {
            "interval" => {
                self.state.lock().unwrap().interval =
                    value.get().expect("type checked upstream");
            }
            _ => unimplemented!(),
        };
    }

    fn property(&self, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            "interval" => self.state.lock().unwrap().interval.to_value(),
            _ => unimplemented!(),
        }
    }
}
impl GstObjectImpl for OriginalBufferSave {}

impl ElementImpl for OriginalBufferSave {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "Original Buffer Save",
                "Generic",
                "Saves a reference to the buffer in a meta",
                "Olivier Crête <olivier.crete@collabora.com>",
            )
        });

        Some(&*ELEMENT_METADATA)
    }

    fn pad_templates() -> &'static [gst::PadTemplate] {
        static PAD_TEMPLATES: Lazy<Vec<gst::PadTemplate>> = Lazy::new(|| {
            let caps = gst::Caps::new_any();
            let src_pad_template = gst::PadTemplate::new(
                "src",
                gst::PadDirection::Src,
                gst::PadPresence::Always,
                &caps,
            )
            .unwrap();
            let sink_pad_template = gst::PadTemplate::new(
                "sink",
                gst::PadDirection::Sink,
                gst::PadPresence::Always,
                &caps,
            )
            .unwrap();

            vec![src_pad_template, sink_pad_template]
        });

        PAD_TEMPLATES.as_ref()
    }

    fn change_state(
        &self,
        transition: gst::StateChange,
    ) -> Result<gst::StateChangeSuccess, gst::StateChangeError> {
        if transition == gst::StateChange::PausedToReady {
            self.state.lock().unwrap().last_running_time = None;
        }

        self.parent_change_state(transition)
    }
}

impl OriginalBufferSave {
    fn forward_query(&self, query: gst::Query) -> Option<gst::Query> {
        let mut s = gst::Structure::new_empty("gst-original-buffer-forward-query");
        s.set("query", query);

        let mut query = gst::query::Custom::new(s);
        if self.src_pad.peer_query(&mut query) {
            let s = query.structure_mut();
            if let (Ok(true), Ok(q)) = (s.get("result"), s.get::<gst::Query>("query")) {
                Some(q)
            } else {
                None
            }
        } else {
            None
        }
    }

    fn sink_chain(
        &self,
        pad: &gst::Pad,
        inbuf: gst::Buffer,
    ) -> Result<gst::FlowSuccess, gst::FlowError> {
        let mut buf = inbuf.copy();
        let caps = pad.current_caps();
        let mut state = self.state.lock().unwrap();

        if state.interval != gst::ClockTime::ZERO {
            if let (Some(segment), Some(pts)) =
                (pad.sticky_event::<gst::event::Segment>(0), buf.pts())
            {
                let rt = segment.segment().to_running_time(pts);
                if let gst::format::GenericFormattedValue::Time(Some(rt)) = rt {
                    if let Some(last_rt) = state.last_running_time {
                        if last_rt + state.interval >= rt {
			    gst::log!(CAT, imp: self,
				      "Dropping buffer and push GAP  pts: {:?} \
				       duration: {:?} because last_rt:{:?} + \
				       interval:{:?} >= rt:{:?}",
				      pts, buf.duration(), last_rt, state.interval, rt);
                            let mut gap = gst::event::Gap::new(pts, buf.duration());
                            let s = gap.make_mut().structure_mut();
                            s.set("original-buffer", buf);
                            if caps.is_some() {
                                s.set("original-caps", caps);
                            }
                            self.src_pad.push_event(gap);
                            return Ok(gst::FlowSuccess::Ok);
                        } else {
			    let mut new_last_rt = last_rt + state.interval;
			    if new_last_rt < rt - state.interval {
				new_last_rt = rt;
			    }
			    state.last_running_time = Some(new_last_rt);
			    gst::log!(CAT, imp: self,
				      "Letting buffer with pts: {:?} through \
				       because last_rt:{:?} + interval:{:?} < rt:{:?}, \
				       updated last_rt to {:?}",
				      pts, last_rt, state.interval, rt,
				      state.last_running_time);
			}
                    } else {
			state.last_running_time = Some(rt);
		    }
                } else {
		    gst::log!(CAT, imp: self, "Can't compute running time");

		}
            } else {
		gst::log!(CAT, imp: self, "No segment event or PTS in buffer");
	    }
        } else {
	    gst::trace!(CAT, imp: self, "Interval is ZERO");
	}

        if let Some(mut meta) = buf.make_mut().meta_mut::<OriginalBufferMeta>() {
            meta.replace(inbuf, caps);
        } else {
            OriginalBufferMeta::add(buf.make_mut(), inbuf, caps);
        }

        self.src_pad.push(buf)
    }

    fn sink_query(
        &self,
        pad: &gst::Pad,
        parent: Option<&impl IsA<gst::Object>>,
        query: &mut gst::QueryRef,
    ) -> bool {
        let ret = gst::Pad::query_default(pad, parent, query);
        if !ret {
            return ret;
        }

        if let gst::QueryViewMut::Caps(q) = query.view_mut() {
            if let Some(caps) = q.result_owned() {
                let forwarding_q = gst::query::Caps::new(Some(&caps)).into();

                if let Some(forwarding_q) = self.forward_query(forwarding_q) {
                    if let gst::QueryView::Caps(c) = forwarding_q.view() {
                        let res = c
                            .result_owned()
                            .map(|c| c.intersect_with_mode(&caps, gst::CapsIntersectMode::First));
                        q.set_result(&res);
                    }
                }
            }
        }

        // We should also do allocation queries, but that requires supporting the same
        // intersection semantics as gsttee, which should be in a helper function.

        true
    }

    fn sink_event(
        &self,
        pad: &gst::Pad,
        parent: Option<&impl IsA<gst::Object>>,
        event: gst::Event,
    ) -> bool {
        match event.view() {
            gst::EventView::Gap(e) => {
                if let Some(s) = e.structure() {
                    if s.has_field("original-buffer") || s.has_field("original-gap") {
                        let (pts, duration) = e.get();
                        let mut gap = gst::event::Gap::new(pts, duration);
                        let s = gap.make_mut().structure_mut();
                        s.set("original-gap", event);
                        return self.src_pad.push_event(gap);
                    }
                }
            }
            gst::EventView::FlushStop(_) => {
                self.state.lock().unwrap().last_running_time = None;
            }
            _ => (),
        }
        gst::Pad::event_default(pad, parent, event)
    }

    fn src_event(
        &self,
        pad: &gst::Pad,
        parent: Option<&impl IsA<gst::Object>>,
        event: gst::Event,
    ) -> bool {
        let event = if event.has_name("gst-original-buffer-forward-upstream-event") {
            event.structure().unwrap().get("event").unwrap()
        } else {
            event
        };

        gst::Pad::event_default(pad, parent, event)
    }
}
